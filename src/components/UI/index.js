import myButton from "@/components/UI/myButton.vue";
import myInput from "@/components/UI/myInput.vue";
import MyDialog from "@/components/UI/MyDialog.vue";
import Navbar from "@/components/UI/Navbar.vue";

export default [
    myButton,
    myInput,
    MyDialog,
    Navbar
]